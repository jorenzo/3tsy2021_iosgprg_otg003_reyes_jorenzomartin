﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float enemyType;
    public GameObject arrow;
    public bool disableOnStart = true;
    public bool inRange = false;
    public int damage = 1;
    public float enemyScore;

    public Vector2 startPos;
    public Vector2 direction;

    public float swipeCond = 75f;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Range")
        {
            GameControlScript.disabled = false;
            disableOnStart = false;
            inRange = true;
        }

        if (col.CompareTag("Player"))
        {
            if (col.GetComponent<Player>().isDamaged == false && col.GetComponent<Player>().isDoingSkill == false)
            {
                col.GetComponent<Player>().health -= damage;
                col.GetComponent<Player>().isDamaged = true;
                Destroy(this.gameObject);
            }
            if(col.GetComponent<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }



    private void Update()
    {
#if UNITY_EDITOR
        InputMouse();
#elif UNITY_ANDROID || UNITY_IOS
        InputTouch();
#endif



    }

    private void LateUpdate()
    {
        if (disableOnStart == true)
        {
            arrow.SetActive(false);
        }
        else
            arrow.SetActive(true);

    }



    void InputMouse()
    {
        //m_Text.text = "Touch : " + message + "in direction" + direction;
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
            //message = "Begun ";
        }

        if (Input.GetMouseButton(0))
        {
            direction = (Vector2)Input.mousePosition - startPos;
            //message = "Tap ";
        }

        // Green Up
        if (Input.GetMouseButton(0) && Input.mousePosition.y > (startPos.y + swipeCond) && enemyType == 1 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            //message = "Swipe Up ";
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }
        // Red Down
        if (Input.GetMouseButton(0) && Input.mousePosition.y > (startPos.y + swipeCond) && enemyType == 5 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }

        // Green Right
        if (Input.GetMouseButton(0) && Input.mousePosition.x > (startPos.x + swipeCond) && enemyType == 2 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }
        // Red Left
        if (Input.GetMouseButton(0) && Input.mousePosition.x > (startPos.x + swipeCond) && enemyType == 6 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }

        // Green Down
        if (Input.GetMouseButton(0) && Input.mousePosition.y < (startPos.y - swipeCond) && enemyType == 3 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }
        // Red Up
        if (Input.GetMouseButton(0) && Input.mousePosition.y < (startPos.y - swipeCond) && enemyType == 7 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }

        // Green Left
        if (Input.GetMouseButton(0) && Input.mousePosition.x < (startPos.x - swipeCond) && enemyType == 4 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }
        // Red Right
        if (Input.GetMouseButton(0) && Input.mousePosition.x < (startPos.x - swipeCond) && enemyType == 8 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }
    }


    void InputTouch()
    {
        //Update the Text on the screen depending on current TouchPhase, and the current direction vector
        //m_Text.text = "Touch : " + message + "in direction" + direction;

        // Track a single touch as a direction control.
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);

            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    // Record initial touch position.
                    startPos = touch.position;
                    //message = "Begun ";
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    // Determine direction by comparing the current touch position with the initial one
                    direction = touch.position - startPos;
                    //message = "Moving ";
                    break;


                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    //message = "Ending ";
                    break;
            }

            if (touch.position.y > (startPos.y + swipeCond) && enemyType == 1 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Up ";
                // GreenUp.SetActive(false);
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }

            if (touch.position.y > (startPos.y + swipeCond) && enemyType == 5 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Up ";
                // RedDown.SetActive(false);
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }


            //swiperight
            if (touch.position.x > (startPos.x + swipeCond) && enemyType == 2 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Right ";
                //GreenRight.SetActive(false); ;
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }

            if (touch.position.x > (startPos.x + swipeCond) && enemyType == 6 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Right ";
                //RedLeft.SetActive(false);
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }


            //swipedown
            if (touch.position.y < (startPos.y - swipeCond) && enemyType == 3 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Down ";
                //  GreenDown.SetActive(false);
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }

            if (touch.position.y < (startPos.y - swipeCond) && enemyType == 7 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Down ";
                // RedUp.SetActive(false);
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }


            //swipeleft
            if (touch.position.x < (startPos.x - swipeCond) && enemyType == 4 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Left ";
                // GreenLeft.SetActive(false);
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }

            if (touch.position.x < (startPos.x - swipeCond) && enemyType == 8 && inRange == true)
            {
                direction = touch.position - startPos;
                //message = "Swipe Left ";
                //RedRight.SetActive(false);
                direction = (Vector2)Input.mousePosition - startPos;
                Destroy(this.gameObject);
                FindObjectOfType<Player>().sp += 1;
                FindObjectOfType<Score>().enemyScore += enemyScore;
                if (FindObjectOfType<Player>().isDoingSkill == true)
                {
                    FindObjectOfType<Player>().sp = 0;
                    Destroy(this.gameObject);
                }
            }
        }


    }

}
