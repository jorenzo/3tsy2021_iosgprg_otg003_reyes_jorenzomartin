﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    bool gameHasEnded = false;

    public void GameOver()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GAME OVER");
            GameOverScreen();
        }
    }

    void GameOverScreen()
    {
        SceneManager.LoadScene("GameOverScreen");
    }

    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }

    public void CharacterSelect()
    {
        SceneManager.LoadScene("CharacterSelection");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
