﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{
    public void Player()
    {
        SceneManager.LoadScene("Main");
    }

    public void Adam()
    {
        SceneManager.LoadScene("Adam");
    }

    public void Lilith()
    {
        SceneManager.LoadScene("Lilith");
    }
}
