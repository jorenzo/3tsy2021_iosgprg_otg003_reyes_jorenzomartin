﻿//Attach this script to an empty GameObject
//Create some UI Text by going to Create>UI>Text.
//Drag this GameObject into the Text field to the Inspector window of your GameObject.

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    //public Enemy[] enemy = new Enemy[8];
    //public GameObject[] enemyPool = new GameObject[8];
    public GameObject player;
    public Enemy enemy1;
    public GameObject enemy;
    GameObject enemyClone;


   // public GameObject GreenUp;
   // GameObject GreenUpClone;
   // public GameObject GreenDown;
   // public GameObject GreenLeft;
   // public GameObject GreenRight;
   // public GameObject RedUp;
    //public GameObject RedDown;
    //public GameObject RedLeft;
    //public GameObject RedRight;
    //public GameObject range;

    public Vector2 startPos;
    public Vector2 direction;

    public TMPro.TextMeshProUGUI m_Text;
    string message;

    public float swipeCond = 75f;

    public bool greenUpInRange = false;
    public bool greenDownInRange = false;
    public bool greenLeftInRange = false;
    public bool greenRightInRange = false;
    public bool redDownInRange = false;
    public bool redUpInRange = false;
    public bool redLeftInRange = false;
    public bool redRightInRange = false;


    void Awake()
    {
        
    }

    private void Start()
    {
        //GreenUpClone = Instantiate(enemyPool[0]);
        //enemyClone= Instantiate(enemy);

    }

    private void Update()
    {
#if UNITY_EDITOR
        InputMouse();
#elif UNITY_ANDROID || UNITY_IOS
        InputTouch();
#endif

    }

    private void LateUpdate()
    {

        
        /*if (enemy[0].disableOnStart == false)
        {
            greenUpInRange = true;
        }

        if (enemy[1].disableOnStart == false)
        {
            greenDownInRange = true;
        }

        if (enemy[2].disableOnStart == false)
        {
            greenLeftInRange = true;
        }

        if (enemy[3].disableOnStart == false)
        {
            greenRightInRange = true;
        }

        if (enemy[4].disableOnStart == false)
        {
            redDownInRange = true;
        }

        if (enemy[5].disableOnStart == false)
        {
            redUpInRange = true;
        }

        if (enemy[6].disableOnStart == false)
        {
            redLeftInRange = true;
        }

        if (enemy[7].disableOnStart == false)
        {
            redRightInRange = true;
        }*/

    }

    public void InputMouse()
    {
        m_Text.text = "Touch : " + message + "in direction" + direction;
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
            message = "Begun ";
        }

        if (Input.GetMouseButton(0))
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Tap ";
        }

        //swipe up
        if (Input.GetMouseButton(0) && Input.mousePosition.y > (startPos.y + swipeCond) && enemy1.enemyType == 1)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Up ";
            //enemy1.KillEnemy();
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.y > (startPos.y + swipeCond) && redDownInRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Up ";
            //RedDown.SetActive(false);
        }

        //swipedown
        if (Input.GetMouseButton(0) && Input.mousePosition.y < (startPos.y - swipeCond) && greenDownInRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Down ";
            //GreenDown.SetActive(false);
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.y < (startPos.y - swipeCond) && redUpInRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Down ";
            //RedUp.SetActive(false);
        }

        //swipeleft
        if (Input.GetMouseButton(0) && Input.mousePosition.x < (startPos.x - swipeCond) && greenLeftInRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Left ";
           // GreenLeft.SetActive(false);
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.x < (startPos.x - swipeCond) && redRightInRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Left ";
           // RedRight.SetActive(false);
        }

        //swiperight
        if (Input.GetMouseButton(0) && Input.mousePosition.x > (startPos.x + swipeCond) && greenRightInRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Right ";
            //GreenRight.SetActive(false); ;
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.x > (startPos.x + swipeCond) && redLeftInRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            message = "Swipe Right ";
            //RedLeft.SetActive(false);
        }

        if (Input.GetMouseButtonUp(0))
        {
            message = "Ending ";
        }

    }

    void InputTouch()
    {
        //Update the Text on the screen depending on current TouchPhase, and the current direction vector
        m_Text.text = "Touch : " + message + "in direction" + direction;

        // Track a single touch as a direction control.
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);

            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    // Record initial touch position.
                    startPos = touch.position;
                    message = "Begun ";
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    // Determine direction by comparing the current touch position with the initial one
                    direction = touch.position - startPos;
                    message = "Moving ";
                    break;


                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    message = "Ending ";
                    break;
            }

            if (touch.position.y > (startPos.y + swipeCond) && greenUpInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Up ";
               // GreenUp.SetActive(false);
            }

            if (touch.position.y > (startPos.y + swipeCond) && redDownInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Up ";
               // RedDown.SetActive(false);
            }

            //swipedown
            if (touch.position.y < (startPos.y - swipeCond) && greenDownInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Down ";
              //  GreenDown.SetActive(false);
            }

            if (touch.position.y < (startPos.y - swipeCond) && redUpInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Down ";
               // RedUp.SetActive(false);
            }

            //swipeleft
            if (touch.position.x < (startPos.x - swipeCond) && greenLeftInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Left ";
               // GreenLeft.SetActive(false);
            }

            if (touch.position.x < (startPos.x - swipeCond) && redRightInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Left ";
                //RedRight.SetActive(false);
            }

            //swiperight
            if (touch.position.x > (startPos.x + swipeCond) && greenRightInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Right ";
                //GreenRight.SetActive(false); ;
            }

            if (touch.position.x > (startPos.x + swipeCond) && redLeftInRange == true)
            {
                direction = touch.position - startPos;
                message = "Swipe Right ";
                //RedLeft.SetActive(false);
            }
        }


    }


}