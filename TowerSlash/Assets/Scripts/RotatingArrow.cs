﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotatingArrow : MonoBehaviour
{
    public float enemyType;
    public GameObject arrow;
    public Sprite[] arrowSprites = new Sprite[4];
    public Sprite realArrow;
    public bool disableOnStart = true;
    public bool inRange = false;
    public int damage = 1;
    public float enemyScore;

    public Vector2 startPos;
    public Vector2 direction;

    public float swipeCond = 75f;


    void Start()
    {
        StartCoroutine(RotateArrow());
    }

    private void Update()
    {

#if UNITY_EDITOR
        InputMouse();
#elif UNITY_ANDROID || UNITY_IOS
        InputTouch();
#endif

    }

    private void LateUpdate()
    {

    }
    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Range")
        {
            GameControlScript.disabled = false;
            disableOnStart = false;
            inRange = true;
        }

        if (col.CompareTag("Player"))
        {
            if (col.GetComponent<Player>().isDamaged == false && col.GetComponent<Player>().isDoingSkill == false)
            {
                col.GetComponent<Player>().health -= damage;
                col.GetComponent<Player>().isDamaged = true;
                Destroy(this.gameObject);
            }
            if (col.GetComponent<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }

    IEnumerator RotateArrow()
    {
        while(!inRange)
        {
            arrow.GetComponent<SpriteRenderer>().sprite = arrowSprites[Random.Range(0, arrowSprites.Length)];
            yield return new WaitForSeconds(.5f);
        }
        if(inRange == true)
        {
            arrow.GetComponent<SpriteRenderer>().sprite = realArrow;
        }
    }

    void InputMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
            //message = "Begun ";
        }

        if (Input.GetMouseButton(0))
        {
            direction = (Vector2)Input.mousePosition - startPos;
            //message = "Tap ";
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.y > (startPos.y + swipeCond) && enemyType == 9 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.x > (startPos.x + swipeCond) && enemyType == 10 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.y < (startPos.y - swipeCond) && enemyType == 11 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }

        if (Input.GetMouseButton(0) && Input.mousePosition.x < (startPos.x - swipeCond) && enemyType == 12 && inRange == true)
        {
            direction = (Vector2)Input.mousePosition - startPos;
            Destroy(this.gameObject);
            FindObjectOfType<Player>().sp += 1;
            FindObjectOfType<Score>().enemyScore += enemyScore;
            if (FindObjectOfType<Player>().isDoingSkill == true)
            {
                FindObjectOfType<Player>().sp = 0;
                Destroy(this.gameObject);
            }
        }
    }

    private void InputTouch()
    {

    }


}
