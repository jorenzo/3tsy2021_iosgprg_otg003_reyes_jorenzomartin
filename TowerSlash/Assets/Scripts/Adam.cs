﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Adam : MonoBehaviour
{

    public int health = 3;
    public int numOfHearts;
    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    public bool isAlive;
    public int sp = 0;
    public int maxSp;

    public Button skillButton;
    public bool buttonIsActive = false;
    public bool isDamaged = false;
    public SpriteRenderer sprite;
    public float skillMod = 10f;
    public float skillDur = 3f;
    public bool isDoingSkill;
    // Start is called before the first frame update

    void Start()
    {
        isAlive = true;
        sprite = GetComponent<SpriteRenderer>();
        skillButton.gameObject.SetActive(false);
        isDoingSkill = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (health > numOfHearts)
        {
            health = numOfHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            if (i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }

        if (health <= 0)
        {
            isAlive = false;
            FindObjectOfType<GameController>().GameOver();
        }

        if (isDamaged == true)
        {
            StartCoroutine(IFrames());
        }

        if (isDamaged == false)
        {
            StopCoroutine(IFrames());
        }

        if (sp >= 5)
        {
            sp = maxSp;
            skillButton.gameObject.SetActive(true);
        }
    }

    public void doSkill()
    {
        skillButton.gameObject.SetActive(false);
        isDoingSkill = true;
        StartCoroutine(DoSkill());
    }

    IEnumerator DoSkill()
    {
        sp = 0;
        FindObjectOfType<Score>().skillScore += 250;

        yield return new WaitForSeconds(3);
        isDoingSkill = false;
        FindObjectOfType<PlayerMovement>().speed = FindObjectOfType<PlayerMovement>().moveSpeed.y;
    }

    IEnumerator IFrames()
    {
        yield return new WaitForSeconds(1.5f);
        isDamaged = false;
    }
}
