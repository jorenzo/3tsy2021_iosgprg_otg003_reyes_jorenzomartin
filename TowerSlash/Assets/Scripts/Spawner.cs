﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public GameObject[] enemy = new GameObject[12];
    public float minTime = 2;
    public float maxTime = 5;
    GameObject enemyClone;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EnemySpawner());
    }


    IEnumerator EnemySpawner()
    {
        while (true)
        {
            enemyClone = Instantiate(enemy[Random.Range(0, enemy.Length)], transform.position, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(minTime, maxTime));
        }
    }

   
}
