﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private float dashSpeed = 5f;

    public GameObject player;
    public GameObject tower;
    public float speed;
    public float playerPos;
    public float score = 0;

    public float dashCount = 0;
    public bool isDashing = false;
    public bool isDoingSkill = false;
    public Vector2 moveSpeed = new Vector2(0, 2f);

    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        playerPos = player.transform.position.y;
        player.transform.Translate(0, speed * Time.deltaTime, 0);
        if (playerPos >= tower.transform.position.y + 5)
        {
            tower.transform.Translate(0, 7.5f, 0);
        }

        if (Input.GetMouseButtonDown(0) && isDashing == false && dashCount == 0)
        {
            isDashing = true;
        }

        if (isDashing == true)
        {
            FindObjectOfType<Score>().dashScore += 6 * Time.deltaTime;
            dashCount += Time.deltaTime;
        }

        if (Input.GetMouseButtonUp(0))
        {
            speed = moveSpeed.y;
            dashCount = 0;
            isDashing = false; 
        }

        if(dashCount == 1)
        {
            speed = moveSpeed.y;
            isDashing = false;
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Began)
            {
                //moveSpeed += dashSpeed;
            }
            if(touch.phase == TouchPhase.Ended)
            {
                //moveSpeed -= dashSpeed;
            }
        }

    }
    
}
