﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float enemyType;
    //public float speed;
    //public float dashSpeed;
    public GameObject arrow;
    public bool disableOnStart = true;

    // Start is called before the first frame update
    void Awake()
    {
        //gameController = GameObject.FindObjectOfType<TestInput>();
        //gameController.KillEnemy(enemyType);

    }

    private void Update()
    {

        if (disableOnStart == true)
        {
            arrow.SetActive(false);
        }
        else
            arrow.SetActive(true);
        

       // transform.Translate(Vector2.down * speed * Time.deltaTime);

        //if (Input.GetMouseButtonDown(0))
        //{
        //    speed += dashSpeed;
        //}
        //if(Input.GetMouseButtonUp(0))
        //{ 
        //    speed -= dashSpeed;
        //}

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Range")
        {
            GameControlScript.disabled = false;
            disableOnStart = false;
        }
    }
}
