﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdamScore : MonoBehaviour
{
    public Transform player;
    public TMPro.TextMeshProUGUI scoreText;
    public float score;
    public float enemyScore;
    public float finalScore;
    public float skillScore;
    public float dashScore;
    //public Text scoreText;

    // Update is called once per frame
    void Update()
    {
        //score += player.position.y;
        score += FindObjectOfType<AdamMovement>().speed * Time.deltaTime;
        finalScore = score + enemyScore + skillScore + dashScore;
        scoreText.text = finalScore.ToString("0");
    }
}
