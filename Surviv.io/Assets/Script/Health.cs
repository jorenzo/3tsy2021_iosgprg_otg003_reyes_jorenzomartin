﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Health : MonoBehaviour
{
    public float CurHealth { get; set; }
    public float MaxHealth { get; set; }
    float maxHealth;

    public void Init(float maxHealth)
    {
        this.maxHealth = maxHealth;
        CurHealth = maxHealth;
    }

    public void AddHealth (float value)
    {
        CurHealth += value;

        if(CurHealth > maxHealth)
        {
            CurHealth = maxHealth;
        }
    }

    public void ApplyDamage(float value)
    {

    }
}
