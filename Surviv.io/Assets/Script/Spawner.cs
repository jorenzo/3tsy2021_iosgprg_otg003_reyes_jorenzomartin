﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform[] spawnLocations;
    public GameObject[] whatToSpawnPrefab;
    public GameObject[] whatToSpawnClone;

    void Start()
    {
        spawnSomething();
    }

    void spawnSomething()
    {
        for (int i = 0; i < spawnLocations.Length; i++)
        {
            whatToSpawnClone[i] = Instantiate(whatToSpawnPrefab[i], spawnLocations[i].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        }
    }
}
