﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb;
    public float moveForce = 10f;

    private FixedJoystick joystick;
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        joystick = GameObject.FindWithTag("Joystick").GetComponent<FixedJoystick>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(joystick.Horizontal * moveForce,
                                  joystick.Vertical * moveForce);

        //if(joystick.Horizontal != 0f || joystick.Vertical != 0f)
        //{
        //    transform.rotation = Quaternion.LookRotation(rb.velocity);
        //}
    }
}
