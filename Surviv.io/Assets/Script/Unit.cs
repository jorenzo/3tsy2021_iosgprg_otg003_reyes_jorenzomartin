﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Unit : MonoBehaviour
{
    public float curHp;
    public float maxHp;
    public Health health;
    public int ammo;
    public float fireRate;
    public float reloadSpeed;

    public Equipment primaryWeapon;
    public Equipment secondaryWeapon;

    public void Init(string nName)
    {

    }

    public void Shoot()
    {
        if(primaryWeapon.weapon != null)
        primaryWeapon.weapon.Fire();
    }

    public void TakeDamage (int damage)
    {
        health.CurHealth = damage;
        if (health.CurHealth <= 0)
            OnDeath();
    }

    void OnDeath()
    {

    }
}
